package JAlexLog;

import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Nexus on 04.03.2017.
 */
public class FileOUT implements Loggable {

    FileOUT(){}

    @Override
    public void log(String msg) {
        try (FileWriter writer = new FileWriter("str.txt", true)) {
            writer.write(msg);
            writer.append('\n');
        } catch (IOException ex) {
            System.out.println("writing error");
        }
    }
}
