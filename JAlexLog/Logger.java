package JAlexLog;

public class Logger implements Runnable {
    private Level level;
    protected FileOUT onFile = null;
    protected SocketOUT onSocket = null;
    protected ConsoleOUT onConsole = null;
    private LogQueue logQueue;
    private Thread logThread;


    public Logger(Level level) {
        this.level = level;
        onFile = new FileOUT();
        onSocket = new SocketOUT();
        onConsole = new ConsoleOUT();
        logQueue = new LogQueue(this);
        logThread = new Thread(this);
        logThread.start();
    }

    public void info(String msg) {
        if (level.getNum() >= Level.INFO.getNum()) {
            logQueue.add("INFO " + msg);
        }
    }
    public void debug(String msg) {
        if (level.getNum() >= Level.DEBUG.getNum()) {
            logQueue.add("DEBUG " + msg);
        }
    }
    public void error(String msg) {
        if (level.getNum() >= Level.ERROR.getNum()) {
            logQueue.add("ERROR " + msg);
        }
    }
    public void setOnFile(boolean i){
        if(i==false)onFile=null;
        else onFile = new FileOUT();
    }
    public void setOnSocket(boolean i){
        if(i==false)onSocket=null;
        else onSocket = new SocketOUT();
    }
    public void setOnConsole(boolean i){
        if(i==false)onConsole=null;
        else onConsole = new ConsoleOUT();
    }
    @Override
    public void run() {

        logQueue = new LogQueue(this);



    }
}
