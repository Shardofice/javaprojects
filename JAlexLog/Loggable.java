package JAlexLog;

/**
 * Created by Nexus on 04.03.2017.
 */
public interface Loggable {
    void log(String msg);
}
