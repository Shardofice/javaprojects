package JAlexLog;

import java.util.Queue;
import java.util.LinkedList;


public class LogQueue {
    private Queue logQueue;
    private Logger mainLog;

    public LogQueue(Logger mainLog) {
        logQueue = new LinkedList();
        this.mainLog = mainLog;
    }

    public void add(String msg) {
        logQueue.add(msg);
        if(mainLog.onConsole != null)
            mainLog.onConsole.log(msg);
        if(mainLog.onFile != null)
            mainLog.onFile.log(msg);
        if(mainLog.onSocket != null)
           mainLog.onSocket.log(msg);

    }

}
