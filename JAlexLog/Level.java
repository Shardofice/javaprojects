package JAlexLog;

/**
 * Created by Nexus on 21.02.2017.
 */
public enum Level {
    INFO(0), DEBUG(1),ERROR(2);
    private int num;
    Level(int n){num=n;}
    int getNum(){return num;}
}

